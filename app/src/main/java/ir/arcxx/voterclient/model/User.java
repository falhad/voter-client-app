package ir.arcxx.voterclient.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Farhad on 2017-03-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class User extends RealmObject {


    @PrimaryKey
    String deviceID;

    public User(String deviceID) {
        this.deviceID = deviceID;
    }


    public User() {
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return getDeviceID().equals(user.getDeviceID());
    }

    @Override
    public int hashCode() {
        return getDeviceID().hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "deviceID='" + deviceID + '\'' +
                '}';
    }
}
