package ir.arcxx.voterclient.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Farhad on 2017-03-07.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Answer extends RealmObject {

    @PrimaryKey
    private String id;
    private String title;
    private RealmList<User> voters;


    public Answer() {
        this.id = UUID.randomUUID().toString();
    }

    public Answer(String title) {
        this.id = UUID.randomUUID().toString();
        this.title = title;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


//    public RealmList<User> getVoters() {
//
//        // Realm  realm = Realm.getDefaultInstance();
//        realm.beginTransaction();
//        voters = new RealmList<>();
//        //tu kole vota oona ke answerID shun ba id e in answer 1 kie user.deviceID ishuno add konam.
//        RealmResults<Vote> allVotesForThisQuestion = realm.where(Vote.class).equalTo("answerID", getId()).findAll();
//        for (Vote v : allVotesForThisQuestion) {
//            voters.add(v.getUser());
//        }
//        realm.commitTransaction();
//        return voters;
//    }


    public RealmList<User> getVoters() {
        return voters;
    }

    public void setVoters(RealmList<User> voters) {
        this.voters = voters;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", voters=" + voters +
                '}';
    }
}
