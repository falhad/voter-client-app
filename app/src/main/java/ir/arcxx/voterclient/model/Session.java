package ir.arcxx.voterclient.model;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Farhad on 2017-07-03.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class Session  extends RealmObject {

    @PrimaryKey
    private String id;
    private Date date;

    public Session() {
        this.id = UUID.randomUUID().toString();
    }


    public Session(Date date) {
        this.id = UUID.randomUUID().toString();
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id='" + id + '\'' +
                ", date=" + date +
                '}';
    }
}
