package ir.arcxx.voterclient.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import ir.arcxx.voterclient.MainActivity;
import ir.arcxx.voterclient.R;
import ir.arcxx.voterclient.model.Answer;
import ir.arcxx.voterclient.model.Question;
import ir.arcxx.voterclient.model.User;
import ir.arcxx.voterclient.model.Vote;
import ir.arcxx.voterclient.utils.FontUtils;
import ir.arcxx.voterclient.utils.PrefUtils;

import static ir.arcxx.voterclient.MainActivity.DEVICE_ID;


/**
 * Created by Farhad on 2017-03-07.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {

    ArrayList<Question> questions;
    Context context;
    ArrayList<QuestionAnswerAdapter> questionAnswerAdapters;
    String TAG = "Question Adapter";
    User clientUser;


    public QuestionAdapter(ArrayList<Question> questions, Context context) {
        this.questions = questions;
        this.context = context;
        this.questionAnswerAdapters = new ArrayList<>();
        clientUser = new User(PrefUtils.loadPref(context, DEVICE_ID));

    }


    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_item, parent, false);
        return new QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {

        Question question = questions.get(position);

        //title
        holder.title.setText(question.getTitle());
        holder.title.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));

        //totalVotes
        int totalVotes = 0;
        for (Answer ans : question.getAnswers()) {
            totalVotes += ans.getVoters().size();
        }

        holder.total_votes_tv.setText(String.format("%s : %s", "تعداد رای ها", totalVotes + ""));
        holder.total_votes_tv.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));

        //answers
        QuestionAnswerAdapter questionAnswerAdapter = new QuestionAnswerAdapter(question, context);
        questionAnswerAdapters.add(position, questionAnswerAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        holder.answers.setLayoutManager(layoutManager);
        holder.answers.setAdapter(questionAnswerAdapter);

        questionAnswerAdapter.setShowResult(userHasVoteThisQuestion(clientUser, question));
        questionAnswerAdapter.notifyDataSetChanged();

        holder.more_iv.setVisibility(View.INVISIBLE);
        //load More


    }

    private boolean userHasVoteThisQuestion(User user, Question question) {

        for (Answer ans : question.getAnswers()) {
            if (ans.getVoters().contains(user)) {
                return true;
            }
        }

        return false;

    }








    @Override
    public int getItemCount() {
        return questions.size();
    }

    class QuestionViewHolder extends RecyclerView.ViewHolder {

        CardView question_cv;
        TextView title;
        RecyclerView answers;
        ImageView more_iv;
        TextView total_votes_tv;

        QuestionViewHolder(View itemView) {
            super(itemView);
            question_cv = itemView.findViewById(R.id.question_cv);
            answers = itemView.findViewById(R.id.question_answer_rv);
            title = itemView.findViewById(R.id.question_title);
            more_iv = itemView.findViewById(R.id.more_iv);
            total_votes_tv = itemView.findViewById(R.id.total_votes_tv);
        }
    }


}
