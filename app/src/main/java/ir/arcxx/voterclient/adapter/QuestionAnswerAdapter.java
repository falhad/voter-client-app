package ir.arcxx.voterclient.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmList;
import ir.arcxx.voterclient.MainActivity;
import ir.arcxx.voterclient.R;
import ir.arcxx.voterclient.model.Answer;
import ir.arcxx.voterclient.model.Question;
import ir.arcxx.voterclient.model.Vote;
import ir.arcxx.voterclient.utils.FontUtils;


/**
 * Created by Farhad on 2017-03-07.
 */

public class QuestionAnswerAdapter extends RecyclerView.Adapter<QuestionAnswerAdapter.QuestionViewHolder> {

    Question question;
    private RealmList<Answer> answers;
    private Context context;
    //    Typeface IranSans;
    String TAG = "QuestionAnswerAdapter";
    boolean showResult = false;

    public boolean isShowResult() {
        return showResult;
    }

    public void setShowResult(boolean showResult) {
        this.showResult = showResult;
    }

    public QuestionAnswerAdapter(Question question, Context context) {
        this.answers = question.getAnswers();
        this.question = question;
        this.context = context;
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.answers_item, parent, false);
        return new QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {

        Answer answer = answers.get(position);

        holder.answer_option_title_tv.setText(answer.getTitle());
        holder.answer_option_title_tv.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));

        holder.answer_vote_count_tv.setText(String.format("%s", String.valueOf(answer.getVoters().size())));
        holder.answer_vote_count_tv.setTypeface(FontUtils.getTypeFace(context, FontUtils.IRANSans));


        if (isShowResult()) {
            holder.answer_vote_selection_iv.setVisibility(View.INVISIBLE);
            holder.answer_vote_count_tv.setVisibility(View.VISIBLE);
        } else {
            holder.answer_vote_selection_iv.setVisibility(View.VISIBLE);
            holder.answer_vote_count_tv.setVisibility(View.INVISIBLE);

            holder.answer_rl.setOnClickListener(v -> {

                holder.answer_vote_selection_iv.setBackgroundResource(R.drawable.selected);

                Vote vote = new Vote(null, question.getId(), answer.getId());

                ViewGroup parentView = ((ViewGroup) holder.answer_rl.getParent());

                for (int i = 0; i < parentView.getChildCount(); i++) {
                    parentView.getChildAt(i).setEnabled(false);
                }

                ((MainActivity) context).submitVote(vote);

            });

        }

    }


    @Override
    public int getItemCount() {
        return answers.size();
    }

    public class QuestionViewHolder extends RecyclerView.ViewHolder {

        TextView answer_vote_count_tv;
        TextView answer_option_title_tv;
        ImageView answer_vote_selection_iv;
        RelativeLayout answer_rl;

        public QuestionViewHolder(View itemView) {
            super(itemView);

            answer_vote_count_tv = itemView.findViewById(R.id.answer_vote_count_tv);
            answer_option_title_tv = itemView.findViewById(R.id.answer_option_title_tv);
            answer_vote_selection_iv = itemView.findViewById(R.id.answer_vote_selection_iv);
            answer_rl = itemView.findViewById(R.id.answer_rl);
        }
    }


}
