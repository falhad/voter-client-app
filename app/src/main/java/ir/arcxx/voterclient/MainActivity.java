package ir.arcxx.voterclient;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.graphics.Typeface;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.koushikdutta.ion.Ion;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.UUID;

import ir.arcxx.voterclient.adapter.QuestionAdapter;
import ir.arcxx.voterclient.model.Question;
import ir.arcxx.voterclient.model.User;
import ir.arcxx.voterclient.model.Vote;
import ir.arcxx.voterclient.utils.FontUtils;
import ir.arcxx.voterclient.utils.PrefUtils;

import static ir.arcxx.voterclient.utils.PrefUtils.getAppVersion;


public class MainActivity extends AppCompatActivity {
    public static final String DEVICE_ID = "DEVICE_ID";


    // TODO: 2017-08-26 prevent send empty port

    Context context;
    String TAG = "MainActivity";

    private CoordinatorLayout coordinatorLayout;
    private ImageView refresh_iv;
    private ImageView drawer_iv;
    private TextView toolbar_title_tv;

    private RecyclerView questionsRV;
    private SwipeRefreshLayout question_srl;
    private ArrayList<Question> questions;
    private QuestionAdapter questionAdapter;
    private RelativeLayout no_questions_exist_rl;
    private Button no_questions_exist_btn;
    private TextView no_questions_exist_tv;


    private String lastPort = "LAST_PORT";

    String serverAddress;
    String serverPort;

    Typeface IranSans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        initUi();
        initDrawer();


    }

    private void initUi() {

        String deviceID = PrefUtils.loadPref(context, DEVICE_ID);

        if (deviceID == null) {
            deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            PrefUtils.savePref(context, DEVICE_ID, deviceID);
        }


        IranSans = FontUtils.getTypeFace(context, FontUtils.IRANSans);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        questions = new ArrayList<>();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);

        coordinatorLayout = findViewById(R.id.coordinator);
        refresh_iv = findViewById(R.id.refresh_iv);
        drawer_iv = findViewById(R.id.drawer_iv);
        toolbar_title_tv = findViewById(R.id.toolbar_title_tv);

        no_questions_exist_rl = findViewById(R.id.no_questions_exist_rl);
        no_questions_exist_btn = findViewById(R.id.no_questions_exist_btn);
        no_questions_exist_tv = findViewById(R.id.no_questions_exist_tv);


        no_questions_exist_btn.setTypeface(IranSans);
        no_questions_exist_tv.setTypeface(IranSans);
        toolbar_title_tv.setTypeface(IranSans);


        questionsRV = findViewById(R.id.questions_rv);
        question_srl = findViewById(R.id.question_srl);


        updateUI();

        questionAdapter = new QuestionAdapter(questions, context);
        questionsRV.setLayoutManager(layoutManager);
        questionsRV.setAdapter(questionAdapter);


        no_questions_exist_btn.setOnClickListener(v -> refreshRequest());

        refresh_iv.setOnClickListener(view -> refreshRequest());

        question_srl.setOnRefreshListener(() -> {

            if (questions != null) {
                question_srl.setRefreshing(true);
                refreshRequest();

            }
        });

        serverAddress = getAccessPointIPAddress(context);

        if (PrefUtils.loadPref(context, lastPort) == null) {
            turnServiceOn();
        } else {
            serverPort = PrefUtils.loadPref(context, lastPort);
            sendConnectRequest();
        }
    }


    private void initDrawer() {

//        PrimaryDrawerItem history = new PrimaryDrawerItem().withTag("history").withName("تاریخچه");
//        PrimaryDrawerItem details = new PrimaryDrawerItem().withTag("details").withName("وضعیت سامانه");
//        PrimaryDrawerItem settings = new PrimaryDrawerItem().withTag("settings").withName("تنظیمات");
        PrimaryDrawerItem about = new PrimaryDrawerItem().withTag("about").withName("درباره برنامه");
        PrimaryDrawerItem exit = new PrimaryDrawerItem().withTag("exit").withName("خروج");

//        history.withTypeface(IranSans);
//        details.withTypeface(IranSans);
//        settings.withTypeface(IranSans);
        about.withTypeface(IranSans);
        exit.withTypeface(IranSans);

//create the drawer and remember the `Drawer` result object
        Drawer result = new DrawerBuilder()
                .withActivity(this)
                .withDrawerGravity(Gravity.RIGHT)
                .withHeader(R.layout.header)
                .withHeaderPadding(false)
                .withSelectedItem(-1)
                .addDrawerItems(
//                        history,
//                        details,
//                        settings,
                        about,
                        exit

                )
                .withOnDrawerItemClickListener((view, position, drawerItem) -> {
                    // do something with the clicked item :D

                    if (drawerItem.getTag().equals("history")) {
//                        showHistory();
                    } else if (drawerItem.getTag().equals("details")) {
//                        showServerStatus();
                    } else if (drawerItem.getTag().equals("settings")) {
//                        showSettings();
                    } else if (drawerItem.getTag().equals("about")) {
                        showAbout();
                    } else if (drawerItem.getTag().equals("exit")) {
                        finish();
                    } else if (drawerItem.getTag().equals("")) {

                    } else {

                    }
                    return false;
                })
                .build();


        drawer_iv.setOnClickListener(v -> result.openDrawer());


        //        serviceStartOrStopIV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                toggleServerStatus();
//            }
//        });
//        showServerStatus.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showServerStatus();
//            }
//        });


//        result.openDrawer();

    }

    private void showAbout() {


        MaterialDialog about = new MaterialDialog.Builder(context)
                .customView(R.layout.about, false)
                .positiveText("خب")
                .onPositive((dialog, which) -> dialog.dismiss())
                .show();


        ViewGroup viewGroup = (ViewGroup) about.getCustomView();
        overrideFonts(context, viewGroup);


    }


    private byte[] convert2Bytes(int hostAddress) {
        byte[] addressBytes = {(byte) (0xff & hostAddress),
                (byte) (0xff & (hostAddress >> 8)),
                (byte) (0xff & (hostAddress >> 16)),
                (byte) (0xff & (hostAddress >> 24))};

        return addressBytes;
    }


    private void turnServiceOn() {

        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {
            MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                    .title("کلمه عبور را وارد کنید.")
                    .titleGravity(GravityEnum.START)
                    .typeface(IranSans, IranSans)
                    .customView(R.layout.password_field, false)
                    .positiveText("خب")
                    .onPositive((dialog, which) -> {

                        String port = ((EditText) dialog.findViewById(R.id.password_et)).getText() + "";
                        serverPort = port;
                        Log.d(TAG, "turnServiceOn: " + serverPort);
                        sendConnectRequest();
                        dialog.dismiss();
                    })
                    .show();

            ((EditText) materialDialog.getCustomView().findViewById(R.id.password_et)).setTypeface(IranSans);

        } else {
            Snackbar.make(coordinatorLayout, "لطفا به شبکه سرور متصل شوید.", Snackbar.LENGTH_LONG).show();

        }

    }


    private void updateUI() {

        if (questions.size() == 0) {
            showNoQuestionExists();
        } else {
            showQuestionsRV();
        }

    }

    private void showQuestionsRV() {
        questionsRV.setVisibility(View.VISIBLE);
        no_questions_exist_rl.setVisibility(View.GONE);
    }

    private void showNoQuestionExists() {
        questionsRV.setVisibility(View.GONE);
        no_questions_exist_rl.setVisibility(View.VISIBLE);
    }


//    ------------------------------- NETWORK REQUESTS!

    private void sendConnectRequest() {

        String connectURL = "http://" + serverAddress + ":" + serverPort + "/connect";
        Log.d(TAG, "sendConnectRequest: Try to send " + connectURL);

        User tempUser = new User(PrefUtils.loadPref(context, DEVICE_ID));

        runOnUiThread(() -> {

            RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(1000);
            rotate.setRepeatCount(999);

            rotate.setInterpolator(new LinearInterpolator());
            refresh_iv.startAnimation(rotate);
        });

        Ion.with(context)
                .load("POST", connectURL)
                .setJsonPojoBody(tempUser)
                .asString()
                .withResponse()
                .setCallback((e, stringResponse) -> {


                    refresh_iv.clearAnimation();

                    if (e != null) {
                        Log.d(TAG, "sendConnectRequest: ");
                        e.printStackTrace();
                        Snackbar.make(coordinatorLayout, "" + String.format("%s خطایی در ارتباط وجود دارد - ", e.getMessage()), Snackbar.LENGTH_LONG).show();

                        turnServiceOn();
                    } else {
                        Log.d(TAG, "sendConnectRequest: " + stringResponse.getResult());
                        if (stringResponse.getResult().equalsIgnoreCase("200")) {
                            PrefUtils.savePref(context, lastPort, serverPort);
                            Snackbar.make(coordinatorLayout, "" + String.format("%s", "متصل"), Snackbar.LENGTH_LONG).show();
                            refreshRequest();
                        } else {
                            Snackbar.make(coordinatorLayout, "" + String.format("%s", "خطا در سرور"), Snackbar.LENGTH_LONG).show();
                            turnServiceOn();
                        }
                    }

                    updateUI();

                });


    }

    private void refreshRequest() {


        String refreshURL = "http://" + serverAddress + ":" + serverPort + "/refresh";

        Log.d(TAG, "refreshRequest: " + refreshURL);

        User tempUser = new User(PrefUtils.loadPref(context, DEVICE_ID));


        runOnUiThread(() -> {

            RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(1000);
            rotate.setRepeatCount(999);

            rotate.setInterpolator(new LinearInterpolator());
            refresh_iv.startAnimation(rotate);
        });

        Ion.with(context)
                .load("POST", refreshURL)
                .setJsonPojoBody(tempUser)
                .asString()
                .withResponse()
                .setCallback((e, stringResponse) -> {

                    refresh_iv.clearAnimation();
                    question_srl.setRefreshing(false);

                    if (e != null) {
                        e.printStackTrace();
                        Snackbar.make(coordinatorLayout, "" + String.format("%s خطایی در ارتباط وجود دارد - ", e.getMessage()), Toast.LENGTH_SHORT).show();

                        if (e.getMessage() != null) {
                            if (e.getMessage().contains("isConnected failed: ECONNREFUSED (Connection refused)")) {
                                turnServiceOn();
                            }
                        }


                    } else {
                        Log.d(TAG, "refreshRequest: " + stringResponse.getResult());
                        ArrayList<Question> responseUser = null;
                        try {
                            responseUser = new ObjectMapper().readValue(stringResponse.getResult(), new TypeReference<ArrayList<Question>>() {
                            });

//                            if (isFinished){
//                                showResults();
//                            }
                            questions.clear();
                            questions.addAll(responseUser);
                            questionAdapter.notifyDataSetChanged();

                        } catch (IOException e1) {
                            e1.printStackTrace();

                            Snackbar.make(coordinatorLayout, "" + String.format("خطا در دریافت اطلاعت، مجدادا تلاش کنید."), Toast.LENGTH_SHORT).show();

                            // inja bade 2,3 sec dobare befreste refreshrequesto?

                        }

                    }

                    updateUI();

                });


    }

    public void submitVote(Vote vote) {

        String submitUrl = "http://" + serverAddress + ":" + serverPort + "/submit";
        Log.d(TAG, "submitVote: " + submitUrl);

        vote.setUser(new User(PrefUtils.loadPref(context, DEVICE_ID)));

        runOnUiThread(() -> {

            RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotate.setDuration(1000);
            rotate.setRepeatCount(999);

            rotate.setInterpolator(new LinearInterpolator());
            refresh_iv.startAnimation(rotate);
        });

        Ion.with(context)
                .load("POST", submitUrl)
                .setJsonPojoBody(vote)
                .asString()
                .withResponse()
                .setCallback((e, stringResponse) -> {
                    refresh_iv.clearAnimation();

                    if (e != null) {
                        if (e.getMessage() != null) {
                            Snackbar.make(coordinatorLayout, "" + String.format("%s خطایی در ارتباط وجود دارد - ", e.getMessage()), Toast.LENGTH_SHORT).show();
                            if (e.getMessage().contains("isConnected failed: ECONNREFUSED (Connection refused)")) {
                                turnServiceOn();
                            }
                        }
                    } else {
                        Log.d(TAG, "submitVote: " + stringResponse.getResult());
                        if (!stringResponse.getResult().equalsIgnoreCase("200")) {
                            Snackbar.make(coordinatorLayout, "" + String.format("مشکلی در ثبت رای به وجود آمد. "), Toast.LENGTH_SHORT).show();
                        }
                        refreshRequest();

                    }
                });


    }

//------------------------------- APP UTILS!


    public String getAccessPointIPAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        byte[] ipAddress = convert2Bytes(dhcpInfo.serverAddress);
        try {
            String apIpAddr = InetAddress.getByAddress(ipAddress).getHostAddress();
            Log.d(TAG, "getAccessPointIPAddress: " + apIpAddr);

            return apIpAddr;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void overrideFonts(final Context context, final View v) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(IranSans);
                ((TextView) v).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                if (((TextView) v).getId() == R.id.version_tv) {
                    ((TextView) v).setText(String.format("نسخه برنامه : %s", getAppVersion(context) + ""));
                }
            }
        } catch (Exception e) {
        }
    }
}
